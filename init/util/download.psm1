Import-Module $BitsTransfer

function Get-RedirectedUrl {
	Param (
		[Parameter(Mandatory=$true)]
		[String]$url
	)
	
	$request = [System.Net.WebRequest]::Create($url)
	$request.AllowAutoRedirect = $false
	$response = $request.GetResponse()
	
	Write-Host ($response | Format-List | Out-String)
	
	if ($response.StatusCode -eq "Found")
	{
		$response.GetResponseHeader("Location")
	}
}

function Download-File($url, $output)
{
	$client = New-Object System.Net.WebClient
	$client.DownloadFile($url, $output)
}