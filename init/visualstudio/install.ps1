$url = "https://aka.ms/vs/17/release/vs_professional.exe"

Import-Module "$PSScriptRoot/../util/download.psm1"

Download-File $url $PSScriptRoot/vs_professional.exe

& "$PSScriptRoot\vs_professional.exe" --config $PSScriptRoot\.vsconfig --wait --passive

