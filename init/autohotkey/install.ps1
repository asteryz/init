Push-Location $PSScriptRoot

git clone https://asteryz@bitbucket.org/asteryz/autohotkey.git
Copy-Item -Path "autohotkey\60%.ahk" -Destination "$env:userprofile\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\60%.ahk"
Remove-Item -Path "autohotkey" -Recurse -Force

Pop-Location
