$ErrorActionPreference = "Stop"

& "$PSScriptRoot/chocolatey/install.ps1"
choco install "$PSScriptRoot/packages.config" --confirm

& "$PSScriptRoot/autohotkey/install.ps1"
& "$PSScriptRoot/git/install.ps1"
& "$PSScriptRoot/hyper/install.ps1"
& "$PSScriptRoot/visualstudio/install.ps1"
& "$PSScriptRoot/vscode/install.ps1"
& "$PSScriptRoot/ssh/configure.ps1"
& "$PSScriptRoot/win10/configure.ps1"
