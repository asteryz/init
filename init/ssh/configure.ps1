choco install putty --confirm

Add-WindowsCapability -Online -Name OpenSSH.Client*
Get-Service -Name ssh-agent | Set-Service -StartupType Manual

refreshenv

$confirmation = Read-Host "Do you want to setup a new id_ed25519 ssh key? [y/n]"
if ($confirmation -eq 'y') {
	mkdir .ssh
    ssh-keygen -t ed25519 -f ".ssh/id_ed25519"
}