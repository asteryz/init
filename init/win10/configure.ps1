Push-Location $PSScriptRoot

git clone https://github.com/Disassembler0/Win10-Initial-Setup-Script.git

& "$PSScriptRoot/Win10-Initial-Setup-Script/Win10.ps1" -include "$PSScriptRoot/Win10-Initial-Setup-Script/Win10.psm1" -preset "$PSScriptRoot/my.preset"

Pop-Location