# README
This repository contains scripts to help initialize my development environments.

## How to use
Open powershell in admin mode in the user folder then execute the following commands:
```
git init
git remote add PATH/TO/THIS/REPO
git fetch
git reset origin/master
git checkout -t origin/master
.\init\install.ps1
```

## It will install
- git
- posh git
- hyper
- python
- everything
- vscode
- vs
- docker
- chrome
- Spotify
- Autohotkey
- Vim

## Windows changes
- Remove search from taskbar

## Settings
- git
- hyper
- vscode
- vstudio

## Create ssh key

## Optional:
- Setup nordvpn
